/******************************************************/
//       THIS IS A GENERATED FILE - DO NOT EDIT       //
/******************************************************/

#include "Particle.h"
#line 1 "/Users/gaurav/Documents/Particle_workbench/CCS811/src/CCS811.ino"
/*
 * Project CCS811
 * Description: firmware testing the CCS811 and comparing it to the SGP30
 * Author: Gaurav
 * Date:
 */

#include "Adafruit_CCS811.h"
#include "Wire.h"
#include "Adafruit_Sensor.h"
#include "Adafruit_SGP30.h"


uint32_t getAbsoluteHumidity(float temperature, float humidity);
void setup();
void loop();
#line 14 "/Users/gaurav/Documents/Particle_workbench/CCS811/src/CCS811.ino"
Adafruit_CCS811 ccs; 
Adafruit_SGP30 sgp;

#define DELAY_TIME 60000 //Delay time in ms
#define SEA_LEVEL_PRESSURE 1013.25 //Used to calculate the altitude of the device in hPa

uint32_t getAbsoluteHumidity(float temperature, float humidity) {
    // approximation formula from Sensirion SGP30 Driver Integration chapter 3.15
    const float absoluteHumidity = 216.7f * ((humidity / 100.0f) * 6.112f * exp((17.62f * temperature) / (243.12f + temperature)) / (273.15f + temperature)); // [g/m^3]
    const uint32_t absoluteHumidityScaled = static_cast<uint32_t>(1000.0f * absoluteHumidity); // [mg/m^3]
    return absoluteHumidityScaled;
}




// setup() runs once, when the device is first turned on.
void setup() {
  // Put initialization like pinMode and begin functions here.
  Serial.println("CCS811 test");
  
  if(!ccs.begin()){
    Serial.println("Failed to start sensor! Please check your wiring.");
    while(1);
  }

  //calibrate temperature sensor
  while(!ccs.available());
  float temp = ccs.calculateTemperature();
  ccs.setTempOffset(temp - 25.0);


  Serial.println("SGP30 test");

  if (! sgp.begin()){
    Serial.println("Sensor not found :(");
    while (1);
  }
  Serial.print("Found SGP30 serial #");
  Serial.print(sgp.serialnumber[0], HEX);
  Serial.print(sgp.serialnumber[1], HEX);
  Serial.println(sgp.serialnumber[2], HEX);

}

void loop() {
 if (! sgp.IAQmeasure()) {
    Serial.println("Measurement failed");
    return;
 }
 

Serial.print("SGP30 eCO2: "); Serial.print(sgp.eCO2); Serial.print(" ppm  ");
Serial.print("TVOC: "); Serial.print(sgp.TVOC); Serial.print(" ppb  ");


  if(ccs.available()){
    float temp = ccs.calculateTemperature();
    if(ccs.readData()){
      Serial.print("CCS811 CO2: ");
      Serial.print(ccs.geteCO2());
      Serial.print("ppm, TVOC: ");
      Serial.print(ccs.getTVOC());
      Serial.print("ppb   Temp:");
      Serial.println(temp);
    }
    else{
      Serial.println("ERROR!");
      while(1);
    }
     

  }
  delay(1000);
}

